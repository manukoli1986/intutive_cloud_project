# Dockerfile 

- This Dockerfile will create a Docker image with the specified dependencies (curl, iproute2, sshfs, unzip, less, and groff) installed. 
- It will also install kubectl and the AWS CLI, making them available in the PATH environment variable. 
- To build the Docker image, save the Dockerfile in a directory, navigate to that directory in the terminal

![Screenshot](img1.png)
![Screenshot](img2.png)

# Terraform

- We will create virtual Private Cloud (VPC) with private and public subnets.
- Will create two Compute Engine instances: one in the public subnet and one in the private subnet.
- Nginx installed and running on the Compute Engine instance in the public subnet, serving a static HTML page.
- The Compute Engine instance in the private subnet should not have direct internet access.

## Steps
- Use Terraform to define and provision the infrastructure resources on GCP.
- Create a GitLab CI pipeline that triggers the infrastructure deployment on every commit to the main branch.
- Ensure that the pipeline fails if there are any syntax or linting errors in the Terraform configuration.
- Store sensitive information (e.g., GCP service account credentials) securely, following best practices.
- Use GitLab CI variables to store and retrieve sensitive information within the pipeline.
- Implement appropriate security measures, such as firewall rules and VPC service controls, to restrict access and enhance the security posture of the infrastructure.

